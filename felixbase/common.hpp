#ifndef FELIXBASE_COMMON_HPP
#define FELIXBASE_COMMON_HPP

#include <set>
#include <chrono>
#include <vector>
#include <string>
#include <random>
#include <functional>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <utility>

#include <sys/types.h>
#include <ifaddrs.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include "felixbase/logging.hpp"

#define TOGBT_BLOCK_BYTES         (16*2)
#define TOGBT_PAYLOAD_BYTES       (15*2)
#define TOGBT_EOM_MASK             0x0001
#define TOGBT_EOM_SHIFT            0
#define TOGBT_LENGTH_MASK          0x001E
#define TOGBT_LENGTH_SHIFT         1
#define TOGBT_EPATH_MASK           0x00E0
#define TOGBT_EPATH_SHIFT          5
#define TOGBT_EGROUP_MASK          0x0700
#define TOGBT_EGROUP_SHIFT         8
#define TOGBT_GBT_MASK             0xF800
#define TOGBT_GBT_SHIFT            11

namespace felix
{
namespace base
{

  template< typename T >
  std::string hex( T i )
  {
    std::stringstream stream;
    stream << "0x"
           << std::setfill ('0') << std::setw(sizeof(T)*2)
           << std::hex << i;
    return stream.str();
  }

  inline void dump_buffer(u_long vaddr, int size, u_long startAddress = 0, u_long virtualAddress = 0)
  {
    INFO("Block Addr:   " << hex(startAddress + vaddr - virtualAddress));
    u_char *buf = (u_char *)vaddr;
    int i;

    for(i = 0; i < size; i++)
    {
      if (i % 32 == 0) {
        printf("\n0x%016lx : ", i + startAddress + vaddr - virtualAddress);
      }
      printf("%02x ", *buf++);
    }
    printf("\n");
  }

  // name: encode_data
  // description: the following code encodes a message.
  // \param  source       data to be encoded.
  // \param  size         maximum number of bytes to be encoded from source.
  // \param  destination  pointer to the destination array where the content is
  //                      to be written.
  // \param  dest_size    size of destiantion.
  // \param  elink        elink number to be written to header.
  // \return If successful, the total number of bytes written is returned.
  //         On failure, -1 is returned.
  inline int encode_data(uint8_t * source,
                         size_t size,
                         char * destination,
                         size_t dest_size,
                         uint64_t elink)
  {
    // Format and write 'size' bytes of test data to the DMA buffer
    int hdr = ((elink  << TOGBT_EPATH_SHIFT)  |
               ((TOGBT_PAYLOAD_BYTES/2) << TOGBT_LENGTH_SHIFT));

    int blocks = (size + TOGBT_PAYLOAD_BYTES-1) / TOGBT_PAYLOAD_BYTES;

    size_t actual_size = blocks * TOGBT_BLOCK_BYTES;

    if (actual_size > dest_size)
    {
      return -1;
    }

    // Fully-filled To-GBT byte blocks
    int dest_i = 0;
    int src_i = 0;
    for(int i=0; i<blocks-1; ++i )
    {
      // Invert byte order
      for(int j=TOGBT_PAYLOAD_BYTES-1; j>=0; --j, ++dest_i )
      {
        destination[dest_i] = source[(src_i + j)] & 0xFF;
      }
      src_i += TOGBT_PAYLOAD_BYTES;
      destination[dest_i++] = (char) (hdr & 0xFF);
      destination[dest_i++] = (char) ((hdr & 0xFF00) >> 8);
    }

    // Final (possibly not fully filled) To-GBT byte block
    int final_size, final_hdr, final_bytes;
    final_bytes = size - (blocks-1) * TOGBT_PAYLOAD_BYTES;
    final_size = (final_bytes + 1) / 2; // Length in 2-byte units
    final_hdr  = hdr & ~TOGBT_LENGTH_MASK;
    final_hdr |= ((final_size << TOGBT_LENGTH_SHIFT) | TOGBT_EOM_MASK);

    // Invert byte order
    for(int j=TOGBT_PAYLOAD_BYTES-1; j>=final_bytes; --j, ++dest_i)
    {
      destination[dest_i] = (char) 0xFF; // Remaining block bytes: 0xFF
    }
    for(int j=final_bytes-1; j>=0; --j, ++dest_i)
    {
      destination[dest_i] = source[(src_i + j)] & 0xFF; // Final data bytes
    }
    destination[dest_i++] = (char) (final_hdr & 0xFF);
    destination[dest_i++] = (char) ((final_hdr & 0xFF00) >> 8);

    // Return the size of the encoded message
    return actual_size;
  }

  // name : parse_range
  // description : Algorithm to extract numbers/number range from a string.
  // \param  #posIntSet  reference to the destination set where the content is to be stored.
  // \param   line  string to be transformed. Ex: 1,5,24-31,34-35,38-39,65,69,88-95,98,99,102-103
  // \return posIntSet is returned.
  inline std::set<unsigned int> parse_range(std::set<unsigned int>& posIntSet, std::string line)
  {
    // Algorithm used:
    //  * Split string using ,
    //  * Split the individual string using -
    //  * Make a range low and high
    //  * Insert it into set with help of this range

    // remove spaces from line
    line.erase( std::remove(line.begin(), line.end(),' '), line.end() );

    std::vector<std::string> strs, r;
    int low, high, base;

    // split string by comma
    boost::split(strs, line, boost::is_any_of(","));

    // Find if a given string conforms to hex notation.
    auto isHexNotation = [](const std::string& s) {
      // Check that the first portion of the string is the literal "0x" and that the remainder of
      // the string contains only the allowed characters.
      return s.compare(0, 2, "0x") == 0
        && s.size() > 2
        && s.find_first_not_of("0123456789abcdefABCDEF", 2) == std::string::npos;
    };

    for (auto it : strs)
    {
      if (it == "" || it[strspn(it.c_str(), "-0x123456789abcdefABCDEF")])
      {
        // empty string or illegal characters found
        continue;
      }

      // split string by dash
      boost::split(r, it, boost::is_any_of("-"));

      base = isHexNotation(r[0]) ? 16 : 10;
      low = high = boost::lexical_cast<int> ( strtol(r[0].c_str(), NULL, base) );

      if(++r.begin() != r.end())
      {
        base = isHexNotation(r[1]) ? 16 : 10;
        high = boost::lexical_cast<int> ( strtol(r[1].c_str(), NULL, base) );
      }

      // high is low and low is high..
      if(low > high)
      {
        high^= low;
        low ^= high;
        high^= low;
      }

      for(int i = low;i <= high; ++i)
      {
        posIntSet.insert(i);
      }
    }

    return posIntSet;
  }

  inline bool oneeditaway(char* s1, char* s2)
  {
    int i, len = strlen(s1);
    bool flag = false;
    for (i = 0; i < len; i++)
    {
      if (s1[i] != s2[i])
      {
        if (flag)
        {
          return false;
        }
        flag = true;
      }
    }
    return true;
  }

  inline bool oneinsertaway(char* s1, char* s2)
  {
    int i1 = 0, i2 = 0, len = strlen(s2);
    while(i2 < len)
      for(i1 = i2 = 0; i1 < len; )
      {
        if(s1[i1] != s2[i2])
        {
          if(i1 != i2)
          {
            return false;
          }
          i2++;
        }
        else
        {
          i1++;
          i2++;
        }
      }
    return true;
  }

  // name : isoneaway
  // description : There are three types of edits that can be performed on strings: insert a
  //               character, remove a character and replace a character.
  // \param  s1  C string to be compared.
  // \param  s2  C string to be compared.
  // \return true if one away, otherwise false.
  inline bool isoneaway(char* s1, char* s2)
  {
    if(strlen(s1) == strlen(s2))
    {
      return oneeditaway(s1, s2);
    }
    else if(strlen(s1) == strlen(s2) - 1)
    {
      return oneinsertaway(s1, s2);
    }
    else if(strlen(s2) == strlen(s1) - 1)
    {
      return oneinsertaway(s2, s1);
    }

    return false;
  }

  // name : populate_random
  // description : Algorithm to fill a vector of n elements with random numbers.
  //               example: a_dist=1 and b_dist=6 -> simulates throwing 6-sided dice.
  // \param  n       # of random numbers.
  // \param  a_dist  an integer value for the lower bound of the distribution.
  // \param  b_dist  an integer value for the upper bound of the distribution.
  // \return vector with random numbers.
  inline std::vector<int> populate_random(int n = 10, int a_dist = 0, int b_dist = 100)
  {
    // First create an instance of an engine.
    std::random_device rnd_device;

    // Specify the engine and distribution.
    std::mt19937 mersenne_engine(rnd_device());
    std::uniform_int_distribution<int> dist(a_dist, b_dist);

    auto gen = std::bind(dist, mersenne_engine);
    std::vector<int> vec(n);

    std::generate(begin(vec), end(vec), gen);
    return vec;
  }

  class Timer
  {
  public:
    Timer()
      : beg_(clock_::now()) {}

    void reset() {
      beg_ = clock_::now();
    }

    double elapsed() const {
      return std::chrono::duration_cast<second_>(clock_::now() - beg_).count();
    }

  private:
    typedef std::chrono::high_resolution_clock clock_;
    typedef std::chrono::duration<double, std::ratio<1> > second_;
    std::chrono::time_point<clock_> beg_;
  };

}
}

#endif
